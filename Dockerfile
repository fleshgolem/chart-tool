FROM tiangolo/uvicorn-gunicorn-fastapi:python3.11
WORKDIR /app
RUN pip install poetry
COPY poetry.lock /app
COPY pyproject.toml /app
RUN poetry config virtualenvs.create false && poetry install
COPY . /app