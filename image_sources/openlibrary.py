from typing import Generic, TypeVar

from pydantic import BaseModel

from image_sources import ChartItem, ImageSource
from main import error_logger


class OLBaseItem(BaseModel):
    key: str
    title: str


DataT = TypeVar("DataT", bound=OLBaseItem)


class OLResult(BaseModel, Generic[DataT]):
    numFound: int
    start: int
    docs: list[DataT]


class OLWorkItem(OLBaseItem):
    editions: OLResult[OLBaseItem]


class OpenLibrarySource(ImageSource):
    async def chart_item_for_input(self, input: str) -> ChartItem | None:
        try:
            res = await self.client.get(
                "https://openlibrary.org/search.json",
                params={"q": input, "fields": "key,title,author_name,editions"},
            )
            if res.status_code == 200:
                result = OLResult[OLWorkItem].model_validate(res.json())
                if len(result.docs) > 0:
                    editions = result.docs[0].editions
                    if len(editions.docs) > 0:
                        edition = editions.docs[0]
                        olid = edition.key.split("/")[-1]
                        cover_url = (
                            f"https://covers.openlibrary.org/b/olid/{olid}-L.jpg"
                        )
                        return ChartItem(title=input, image_url=cover_url)
                    return None
                return None
            else:
                print(f"Error searching {input}: {res.status_code}")
                return None
        except Exception as e:
            error_logger.warn(f"Error retrieving book cover for {input}: {e}")
            return None
