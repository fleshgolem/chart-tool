from pydantic import BaseModel

from config import Config
from image_sources import ChartItem, ImageSource

config = Config()


class TMDMovie(BaseModel):
    id: int
    title: str
    poster_path: str | None


class TMDResult(BaseModel):
    page: int
    total_results: int
    results: list[TMDMovie]


class TheMovieDBSource(ImageSource):
    async def chart_item_for_input(self, input: str) -> ChartItem | None:
        res = await self.client.get(
            "https://api.themoviedb.org/3/search/movie",
            params={"query": input},
            headers={"Authorization": f"Bearer {config.THEMOVIEDB_KEY}"},
        )
        if res.status_code == 200:
            result = TMDResult.model_validate(res.json())
            if len(result.results) > 0 and result.results[0].poster_path:
                poster_path = result.results[0].poster_path
                cover_url = f"https://image.tmdb.org/t/p/w342/{poster_path}"
                return ChartItem(title=input, image_url=cover_url)
            return None
        else:
            print(f"Error searching {input}: {res.status_code}")
            return None
