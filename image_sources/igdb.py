from datetime import datetime, timedelta
from typing import Annotated, Optional

import backoff
from pydantic import BaseModel, Field, RootModel

from config import Config
from image_sources import ImageSource, ChartItem

config = Config()


class IGDBCover(BaseModel):
    id: int
    height: int
    image_id: str
    width: int


class IGDBResponse(BaseModel):
    id: int
    cover: Optional[IGDBCover] = None
    name: str
    total_rating_count: int = 0


class AccessTokenResponse(BaseModel):
    access_token: str
    expires_in: int
    token_type: str
    received: Annotated[datetime, Field(default_factory=datetime.utcnow)]

    @property
    def is_fresh(self) -> bool:
        return self.received + timedelta(seconds=self.expires_in) > datetime.utcnow()


class IGDBSource(ImageSource):
    async def _get_stored_access_data(self) -> AccessTokenResponse | None:
        data = await self.redis.get("Access:IGDB")
        if not data:
            return None
        try:
            s = AccessTokenResponse.model_validate_json(data)
            if s.is_fresh:
                return s
            return None

        except Exception as e:
            print(f"Could not validate cached access token: {e}")
            return None

    async def _store_access_data(self, data: AccessTokenResponse) -> None:
        await self.redis.set("Access:IGDB", data.model_dump_json())

    async def _get_access_token(self) -> AccessTokenResponse:
        if stored := await self._get_stored_access_data():
            return stored
        res = await self.client.post(
            "https://id.twitch.tv/oauth2/token",
            params={
                "client_id": config.TWITCH_CLIENT_ID,
                "client_secret": config.TWITCH_CLIENT_SECRET,
                "grant_type": "client_credentials",
            },
        )
        res.raise_for_status()
        token_res = AccessTokenResponse.model_validate_json(res.text)
        await self._store_access_data(token_res)
        return token_res

    @backoff.on_predicate(
        backoff.runtime,
        predicate=lambda r: r.status_code == 429,
        value=lambda r: int(r.headers.get("Retry-After") or "1"),
        jitter=None,
    )
    async def _igdb_request(self, input: str, access_token: AccessTokenResponse):
        return await self.client.post(
            "https://api.igdb.com/v4/games",
            content=f'search "{input}"; fields name, cover.*, artworks, total_rating_count; limit 20;',
            headers={
                "Accept": "application/json",
                "Authorization": f"Bearer {access_token.access_token}",
                "Client-ID": config.TWITCH_CLIENT_ID,
            },
        )

    async def chart_item_for_input(self, input: str) -> ChartItem | None:
        lock = self.redis.lock("Access:IGDB:Lock")
        await lock.acquire(blocking=True, blocking_timeout=5)
        access_token = await self._get_access_token()
        await lock.release()

        res = await self._igdb_request(input, access_token)
        res.raise_for_status()
        model = RootModel[list[IGDBResponse]].model_validate_json(res.text)
        if len(model.root) > 0:
            best_fit = [m for m in model.root if m.name.lower() == input.lower()]
            best_fit = sorted(
                best_fit, key=lambda m: m.total_rating_count, reverse=True
            )
            if len(best_fit) > 0 and best_fit[0].cover:
                img_url = f"https://images.igdb.com/igdb/image/upload/t_cover_big/{best_fit[0].cover.image_id}.jpg"
            elif model.root[0].cover:
                img_url = f"https://images.igdb.com/igdb/image/upload/t_cover_big/{model.root[0].cover.image_id}.jpg"
            else:
                return None
            return ChartItem(title=input, image_url=img_url)
        return None
