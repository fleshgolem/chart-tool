from enum import Enum
from typing import Annotated

import backoff
import httpx
from pydantic import BaseModel, Field

from config import Config
from image_sources import ChartItem, ImageSource, fatal_code


class LFMError(Exception):
    class Type(Enum):
        UNKNOWN_ERROR = 0
        USER_NOT_FOUND = 1
        NO_TRACK = 2
        NO_PLAYS = 3
        UNREACHABLE = 4

    def __init__(self, type: Type):
        self.type = type


config = Config()


class Size(Enum):
    EXTRALARGE = "extralarge"
    LARGE = "large"
    MEDIUM = "medium"
    SMALL = "small"


class LFMImage(BaseModel):
    size: Size
    text: Annotated[str | None, Field(None, alias="#text")]


class LFMAlbum(BaseModel):
    artist: str
    image: list[LFMImage]
    url: str
    name: str
    mbid: str | None = None


class LFMAlbumMatches(BaseModel):
    album: list[LFMAlbum]


class LastFMSource(ImageSource):
    base_url = "https://ws.audioscrobbler.com/2.0/"

    @backoff.on_exception(backoff.expo, httpx.ReadTimeout, max_tries=config.MAX_RETRIES)
    @backoff.on_exception(
        backoff.expo,
        httpx.HTTPStatusError,
        max_tries=config.MAX_RETRIES,
        giveup=fatal_code,
    )
    async def lfmrequest(self, command: str, params=None):
        if params is None:
            params = {}
        params["method"] = command
        params["format"] = "json"
        params["api_key"] = config.LASTFM_KEY
        r = await self.client.get(self.base_url, params=params)
        r.raise_for_status()
        return r

    async def search_album(self, album: str, limit: int = 1) -> LFMAlbum | None:
        try:
            response = await self.lfmrequest(
                " album.search", {"album": album, "limit": limit}
            )
            data = response.json()
            matches = LFMAlbumMatches.model_validate(data["results"]["albummatches"])
            if len(matches.album) > 0:
                return matches.album[0]
            else:
                return None
        except Exception as e:
            print(f"Error searching {album}: {e}")
            return None

    async def chart_item_for_input(self, input: str) -> ChartItem | None:
        lfm_album = await self.search_album(input, 1)
        if lfm_album and lfm_album.image[3].text:
            url = lfm_album.image[3].text
            if (
                url
                == "https://lastfm.freetls.fastly.net/i/u/300x300/2a96cbd8b46e442fc41c2b86b821562f.png"
            ):
                return None
            return ChartItem(title=input, image_url=lfm_album.image[3].text)
        return None
