from typing import Generic, TypeVar

import backoff
import httpx
from bs4 import BeautifulSoup
from pydantic import BaseModel
from pydantic_xml import BaseXmlModel, attr, wrapped, element

from config import Config
from image_sources import ChartItem, ImageSource, fatal_code
from main import error_logger


class BGGSearchItem(BaseXmlModel):
    id: int = attr("id")
    name: str = wrapped("name", attr("value"))


class BGGSearchResult(BaseXmlModel, tag="items"):
    total: int = attr(default=0)
    items: list[BGGSearchItem] = element(tag="item", default_factory=list)


class BGGImage(BaseXmlModel):
    url: str


config = Config()


class BoardGameGeekImageSource(ImageSource):

    @backoff.on_exception(backoff.expo, httpx.ReadTimeout, max_tries=config.MAX_RETRIES)
    @backoff.on_exception(
        backoff.expo,
        httpx.HTTPStatusError,
        max_tries=config.MAX_RETRIES,
        giveup=fatal_code,
    )
    async def chart_item_for_input(self, input: str) -> ChartItem | None:
        res = await self.client.get(
            "https://boardgamegeek.com/xmlapi2/search",
            params={"query": input, "type": "boardgame"},
        )
        res.raise_for_status()
        m = BGGSearchResult.from_xml(res.content)
        if len(m.items) > 0:
            first = m.items[0]
            inner_res = await self.client.get(
                "https://boardgamegeek.com/xmlapi2/thing",
                params={"id": first.id},
            )
            inner_res.raise_for_status()
            # For some reason this just doesnt work with pydantic-lxml, so revert back to good ol bs4
            soup = BeautifulSoup(inner_res.content, "lxml-xml")
            image_tag = soup.find("image")
            if image_tag:
                return ChartItem(title=input, image_url=image_tag.text)
        return None
