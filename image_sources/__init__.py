from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum

import httpx
from httpx import AsyncClient
from redis.asyncio import Redis


@dataclass
class ChartItem:
    title: str
    image_url: str


class ImageSource(ABC):
    def __init__(self, client: AsyncClient, redis: Redis):
        self.client = client
        self.redis = redis

    @abstractmethod
    async def chart_item_for_input(self, input: str) -> ChartItem | None:
        pass


class ImageSourceType(Enum):
    lastfm = "lastfm"
    openlibrary = "openlibrary"
    themoviedb = "themoviedb"
    igdb = "igdb"
    bgg = "bgg"

    def source(self, client: AsyncClient, redis: Redis) -> ImageSource:
        from image_sources.lastfm import LastFMSource
        from image_sources.openlibrary import OpenLibrarySource
        from image_sources.themoviedb import TheMovieDBSource
        from image_sources.igdb import IGDBSource
        from image_sources.boardgamegeek import BoardGameGeekImageSource

        return {
            ImageSourceType.lastfm: LastFMSource(client, redis),
            ImageSourceType.openlibrary: OpenLibrarySource(client, redis),
            ImageSourceType.themoviedb: TheMovieDBSource(client, redis),
            ImageSourceType.igdb: IGDBSource(client, redis),
            ImageSourceType.bgg: BoardGameGeekImageSource(client, redis),
        }[self]

    @property
    def display_value(self) -> str:
        return {
            ImageSourceType.lastfm: "Last.fm (Music)",
            ImageSourceType.openlibrary: "Open Library (Books)",
            ImageSourceType.themoviedb: "TMDB (Movies)",
            ImageSourceType.igdb: "Internet Games Database (Video Games)",
            ImageSourceType.bgg: "BoardGameGeek (Board Games)",
        }[self]

    @staticmethod
    def all() -> list["ImageSourceType"]:
        return [
            ImageSourceType.lastfm,
            ImageSourceType.openlibrary,
            ImageSourceType.themoviedb,
            ImageSourceType.igdb,
            ImageSourceType.bgg,
        ]


def fatal_code(e: Exception):
    if isinstance(e, httpx.HTTPStatusError):
        return 400 <= e.response.status_code < 500 and e.response.status_code != 429
    return False
