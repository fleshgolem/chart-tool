import asyncio
import math
from collections.abc import Sequence
from dataclasses import dataclass
from io import BytesIO

from fastapi import HTTPException
from httpx import AsyncClient
from PIL import Image, ImageDraw, ImageFont
from redis.asyncio import Redis

from helper import chunks
from image_downloader import AlbumImageData, download_image
from image_sources import ImageSourceType
from model import Album

IMAGE_SIZE = 300
COVER_TEXT_PADDING = 20
INTER_IMAGE_PADDING = 8
OFFSET = 10


@dataclass
class RenderingException(Exception):
    body: str


async def get_all_album_image(
    albums: list[Album], source_type: ImageSourceType, redis: Redis, client: AsyncClient
) -> Sequence[AlbumImageData]:
    coros = [download_image(a, source_type, redis, client) for a in albums]
    return await asyncio.gather(*coros)


def text_space_for_names(names: list[str], columns: int):
    longest_name = ""
    for a in names:
        if len(a) > len(longest_name):
            longest_name = a
    return int(fnt.getlength(longest_name) + 45)


async def generate_chart(
    albums: list[Album],
    source_type: ImageSourceType,
    column_count: int,
    redis: Redis,
    client: AsyncClient,
) -> BytesIO:
    images = await get_all_album_image(albums, source_type, redis, client)
    album_names: list[str] = [a.title for a in albums]
    row_count = math.ceil(len(albums) / column_count)
    text_space = text_space_for_names(album_names, column_count)

    if column_count < 7:
        return draw_chart(
            album_names, column_count, images, row_count, text_space, False
        )

    rows = math.ceil(len(albums) / column_count)
    left_columns = column_count // 2
    right_colums = column_count - left_columns

    left_names = []
    right_names = []
    left_images: list[AlbumImageData] = []
    right_images: list[AlbumImageData] = []

    for i in range(rows):
        base = i * column_count
        left_names.extend(album_names[base : base + left_columns])
        right_names.extend(
            album_names[(base + left_columns) : base + left_columns + right_colums]
        )
        left_images.extend(images[base : base + left_columns])
        right_images.extend(
            images[(base + left_columns) : base + left_columns + right_colums]
        )

    left_text_space = text_space_for_names(left_names, left_columns)
    right_text_space = text_space_for_names(right_names, right_colums)

    left_image = Image.open(
        draw_chart(
            left_names,
            left_columns,
            left_images,
            row_count,
            left_text_space,
            True,
        )
    )
    right_image = Image.open(
        draw_chart(
            right_names,
            right_colums,
            right_images,
            row_count,
            right_text_space,
            False,
        )
    )
    size = (
        left_image.size[0] + right_image.size[0] + COVER_TEXT_PADDING,
        max(left_image.size[1], right_image.size[1]),
    )
    canvas = Image.new("RGBA", size, (0, 0, 0, 255))
    canvas.paste(left_image, (0, 0))
    canvas.paste(right_image, (left_image.size[0] + COVER_TEXT_PADDING * 2, 0))

    io = BytesIO()
    canvas.save(io, "PNG")
    io.seek(0)
    return io


def draw_chart(album_names, column_count, images, row_count, text_space, flipped=False):
    size = (
        column_count * IMAGE_SIZE + COVER_TEXT_PADDING * 3 + text_space,
        row_count * IMAGE_SIZE + COVER_TEXT_PADDING * 3,
    )
    canvas = Image.new("RGBA", size, (0, 0, 0, 255))
    if flipped:
        offset = text_space + COVER_TEXT_PADDING * 2
        text_offset = COVER_TEXT_PADDING
    else:
        offset = COVER_TEXT_PADDING
        text_offset = (
            column_count * IMAGE_SIZE
            + COVER_TEXT_PADDING * 2
            + INTER_IMAGE_PADDING * column_count
        )

    draw_covers_on_canvas(canvas, images, column_count, offset)
    draw_names_on_canvas(
        canvas,
        album_names,
        column_count,
        text_offset,
    )
    io = BytesIO()
    canvas.save(io, "PNG")
    io.seek(0)
    return io


fnt = ImageFont.truetype("static/fonts/arial-unicode-ms.ttf", 36)


def draw_names_on_canvas(canvas, album_names, column_count, offset):
    text_chunks = chunks(album_names, column_count)
    d = ImageDraw.Draw(canvas)
    for row_idx, row in enumerate(text_chunks):
        for col_idx, text in enumerate(row):
            d.text(
                (
                    offset,
                    row_idx * IMAGE_SIZE
                    + INTER_IMAGE_PADDING * row_idx
                    + col_idx * 40
                    + COVER_TEXT_PADDING / 3,
                ),
                text,
                font=fnt,
                fill=(255, 255, 255, 255),
            )


def draw_covers_on_canvas(canvas, images: list[AlbumImageData], count, offset):
    image_chunks = chunks(images, count)
    for row_idx, row in enumerate(image_chunks):
        for col_idx, album_image in enumerate(row):
            image = album_image.image

            target_size = (IMAGE_SIZE, IMAGE_SIZE)
            resized = image
            resized.thumbnail(target_size)
            resized = resized.convert("RGBA")
            canvas.paste(
                resized,
                (
                    col_idx * IMAGE_SIZE + INTER_IMAGE_PADDING * col_idx + offset,
                    row_idx * IMAGE_SIZE
                    + INTER_IMAGE_PADDING * row_idx
                    + COVER_TEXT_PADDING,
                ),
                resized,
            )


def bot_check(lines: list[str]):
    lines = [l for l in lines if l.strip() != ""]
    sentences = [l.split(" ") for l in lines if len(l.split(" ")) >= 15]
    if len(sentences) >= len(lines) - len(sentences) or len(sentences) > 5:
        raise HTTPException(status_code=400, detail="You are a bot, arent you?")
    if len(sentences) > 0 and lines[0].lower().startswith("hi"):
        raise HTTPException(status_code=400, detail="You are a bot, arent you?")
    if len(sentences) > 0 and any([l for l in lines if " seo " in l.lower()]):
        raise HTTPException(status_code=400, detail="You are a bot, arent you?")
    really_long_sentences = [l.split(" ") for l in lines if len(l.split(" ")) >= 50]
    if len(really_long_sentences) >= 1:
        raise HTTPException(status_code=400, detail="You are a bot, arent you?")


async def render_input_to_chart(
    input: str,
    source_type: ImageSourceType,
    columns: int,
    redis: Redis,
    client: AsyncClient,
) -> BytesIO:
    input = input.replace("\r\n", "\n")
    lines = [a for a in input.split("\n") if a.strip() != ""]

    bot_check(lines)
    albums = [Album.parse(l) for l in lines]
    columns = max(min(columns, 12), 1)
    return await generate_chart(albums, source_type, columns, redis, client)
