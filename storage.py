from redis.asyncio import Redis

from config import Config
from image_sources import ImageSourceType

config = Config()


async def _get_str(key: str, redis: Redis) -> str | None:
    if config.DISABLE_CACHE:
        return None
    v = await redis.get(key)
    return str(v, "utf-8") if v else None


async def cache_album_image(
    title: str, image: bytes, source_type: ImageSourceType, redis: Redis
):
    key = f"ImageCache:{source_type.value}:{title}"
    await redis.set(key, image)
    await redis.expire(key, 60 * 60 * 24 * 7)


async def get_cached_album_image(
    title: str, source_type: ImageSourceType, redis: Redis
) -> bytes | None:
    if config.DISABLE_CACHE:
        return None
    key = f"ImageCache:{source_type.value}:{title}"
    return await redis.get(key)


async def delete_cached_album_image(
    title: str, source_type: ImageSourceType, redis: Redis
):
    key = f"ImageCache:{source_type.value}:{title}"
    await redis.delete(key)


async def cache_url_image(url: str, image: bytes, redis: Redis):
    key = f"ImageCache:URL:{url}"
    await redis.set(key, image)
    await redis.expire(key, 60 * 60 * 24 * 7)


async def get_cached_url_image(url: str, redis: Redis) -> bytes | None:
    if config.DISABLE_CACHE:
        return None
    key = f"ImageCache:URL:{url}"
    return await redis.get(key)


async def delete_cached_url_image(url: str, redis: Redis):
    key = f"ImageCache:URL:{url}"
    return await redis.delete(key)
