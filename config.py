from pydantic_settings import BaseSettings, SettingsConfigDict


class Config(BaseSettings):
    LASTFM_KEY: str = ""
    THEMOVIEDB_KEY: str = ""
    TWITCH_CLIENT_ID: str = ""
    TWITCH_CLIENT_SECRET: str = ""
    CACHE_REDIS_URL: str = ""
    DISABLE_CACHE: bool = False
    DEBUG: bool = False
    MAX_RETRIES: int = 3
    SENTRY_DSN: str | None = None
    SENTRY_ENV: str = "dev"
    SENTRY_TRACE_RATE: float = 0.0
    model_config = SettingsConfigDict(env_file=".env", extra="ignore")
