import asyncio
from dataclasses import dataclass
from io import BytesIO

from httpx import AsyncClient
from PIL import Image
from redis.asyncio import Redis

from config import Config
from image_sources import ImageSourceType
from model import Album
from storage import (
    cache_album_image,
    cache_url_image,
    delete_cached_album_image,
    delete_cached_url_image,
    get_cached_album_image,
    get_cached_url_image,
)

config = Config()


@dataclass
class AlbumImageData:
    title: str
    image: Image.Image
    image_bytes: bytes


async def download_image(
    album: Album, source_type: ImageSourceType, redis: Redis, client: AsyncClient
) -> AlbumImageData:
    d = None
    if album.image_url:
        d = await download_image_url(album.image_url, album.title, redis, client)
    else:
        d = await download_image_from_source(album.title, source_type, redis, client)

    if d:
        return d
    else:
        return placeholder_image_data(album.title)


async def _download_image_file(
    url: str, client: AsyncClient, retrycount=0
) -> bytes | None:
    try:
        r = await client.get(url.strip(), follow_redirects=True)
        if r.status_code != 200:
            return None
        return r.content
    except Exception as e:
        if retrycount < config.MAX_RETRIES:
            await asyncio.sleep(0.2)
            return await _download_image_file(url, client, retrycount + 1)
        raise e


async def download_image_url(
    url: str, title: str, redis: Redis, client: AsyncClient
) -> AlbumImageData | None:
    cached = await get_cached_url_image(url, redis)
    if cached:
        try:
            img = load_image(cached)
            return AlbumImageData(title=title, image=img, image_bytes=cached)
        except Exception:
            await delete_cached_url_image(url, redis)
            return await download_image_url(url, title, redis, client)
    downloaded = await _download_image_file(url, client)
    if downloaded:
        try:
            img = load_image(downloaded)
            await cache_url_image(url, downloaded, redis)

            return AlbumImageData(title=title, image=img, image_bytes=downloaded)
        except Exception:
            return None
    return None


async def download_image_from_source(
    title: str, source_type: ImageSourceType, redis: Redis, client: AsyncClient
) -> AlbumImageData | None:
    cached = await get_cached_album_image(title, source_type, redis)
    if cached:
        try:
            img = load_image(cached)
            return AlbumImageData(title=title, image=img, image_bytes=cached)
        except Exception:
            await delete_cached_album_image(title, source_type, redis)
            return await download_image_from_source(title, source_type, redis, client)

    item = await source_type.source(client, redis).chart_item_for_input(title)
    if not item:
        return None
    image_data = await download_image_url(item.image_url, item.title, redis, client)
    if image_data:
        await cache_album_image(title, image_data.image_bytes, source_type, redis)
        return image_data
    return None


def load_image(bytes: bytes):
    io = BytesIO(bytes)
    return Image.open(io)


def placeholder_image_data(title) -> AlbumImageData:
    placeholder_file = open("static/img/cancel.png", "rb")
    image = Image.open(placeholder_file)
    image = image.resize((500, 500))
    placeholder_file.seek(0)
    bytes = placeholder_file.read()
    return AlbumImageData(title=title, image=image, image_bytes=bytes)
