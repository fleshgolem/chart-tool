from pydantic import BaseModel


def sanitize(input: str) -> str:
    if input.startswith("-"):
        input = input[1:]
    if input.startswith("*"):
        input = input[1:]
    if input.startswith("•"):
        input = input[1:]
    if input.endswith("."):
        input = input[:-1]
    return input.strip()


class Album(BaseModel):
    title: str
    image_url: str | None = None

    @staticmethod
    def parse(input: str) -> "Album":
        input = sanitize(input)
        if "|" in input:
            parts = input.split("|", maxsplit=1)
            return Album(title=parts[0].strip(), image_url=parts[1].strip())
        else:
            return Album(title=input.strip(), image_url=None)
