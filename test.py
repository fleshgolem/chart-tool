import asyncio

import aiofiles
from httpx import AsyncClient
from redis.asyncio import Redis

from chart import render_input_to_chart
from image_sources import ImageSourceType


async def render_test(input: str, columns: int = 5):
    redis = Redis(host="localhost")
    client = AsyncClient()
    io = await render_input_to_chart(
        input, ImageSourceType.lastfm, columns, redis, client
    )
    filename = "output.png"
    async with aiofiles.open(filename, mode="wb") as f:
        await f.write(io.read())


async def test_1():
    input = """
    Cozy|https://fanfiaddict.com/wp-content/uploads/2022/06/Legends-and-Lattes.png
    """
    await render_test(input)


async def test_2():
    input = """
    my favorite books|https://fanfiaddict.com/wp-content/uploads/2022/06/Legends-and-Lattes.png
    my favorite books|https://fanfiaddict.com/wp-content/uploads/2022/06/Legends-and-Lattes.png
    my favorite books|https://fanfiaddict.com/wp-content/uploads/2022/06/Legends-and-Lattes.png
    my favorite books|https://fanfiaddict.com/wp-content/uploads/2022/06/Legends-and-Lattes.png
    my favorite books|https://fanfiaddict.com/wp-content/uploads/2022/06/Legends-and-Lattes.png
    """
    await render_test(input)


async def test_3():
    input = """
    These are|https://fanfiaddict.com/wp-content/uploads/2022/06/Legends-and-Lattes.png
    my favorite| https://www.bookcity.pl/bigcovers/5/2/8/1/9781250245281.jpg
books and I|https://m.media-amazon.com/images/I/51jXSDvfLOL.jpg
really hope|https://www.themost10.com/wp-content/uploads/2012/06/How-To-Avoid-Huge-Ships.jpg
everything aligns better now|https://images-ext-1.discordapp.net/external/9y1XCEPq52p65ZxZjpyQVX196G0UNtELUZRrjAUme2g/https/www.cbc.ca/strombo/content/images/worst-book-covers-time-ninja.jpg?format=webp&width=407&height=573
"""
    await render_test(input)


async def test_4():
    input = """
Tyrann - Besatt
Blood Ceremony - The Old Ways Remain
Oozing Wound - We cater to Cowards
Kostnatění - Úpal
Century -  The Conquest of Time 
Anthropophagous - Abuse of a Corpse
Gates of Dawn - II
Vril - Animist
Sari - 大団円
Voyage Futur - Wellen
Augnos - Gilded Sentience
Rampue - Bubblebath Trance
Tim Hecker - No Highs
"""
    await render_test(input)


if __name__ == "__main__":

    async def _run():
        await test_4()

    asyncio.run(_run())
