import logging
from collections.abc import AsyncIterator
from contextlib import asynccontextmanager
from datetime import datetime
from logging import Logger
from logging.handlers import RotatingFileHandler
from os import path
from typing import Annotated, Callable, Literal
from uuid import uuid4, UUID

import aiofiles
import sentry_sdk
from fastapi import Depends, FastAPI, Form, Request, HTTPException, BackgroundTasks
from fastapi.responses import HTMLResponse
from fastapi.routing import APIRoute, APIRouter
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi_limiter import FastAPILimiter
from fastapi_limiter.depends import RateLimiter
from httpx import AsyncClient
from pydantic import BaseModel, ValidationError
from redis.asyncio import ConnectionPool, Redis
from starlette import status
from starlette.responses import Response
from uvicorn import run  # type: ignore

from chart import render_input_to_chart
from config import Config
from image_sources import ImageSourceType

request_logger = logging.getLogger("chart-tool:requests")
request_logger.setLevel(logging.INFO)

request_logger.addHandler(
    RotatingFileHandler("logs/requests.log", maxBytes=1024 * 1024 * 10, backupCount=10)
)
request_logger.addHandler(logging.StreamHandler())
config = Config()

if config.SENTRY_DSN:
    sentry_sdk.init(
        dsn=config.SENTRY_DSN,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        traces_sample_rate=config.SENTRY_TRACE_RATE,
        # Set profiles_sample_rate to 1.0 to profile 100%
        # of sampled transactions.
        # We recommend adjusting this value in production.
        profiles_sample_rate=config.SENTRY_TRACE_RATE,
        environment=config.SENTRY_ENV,
    )


class LoggingRoute(APIRoute):
    def get_route_handler(self) -> Callable:

        async def _log_request(request: Request, status_code: int):
            if request.method in ["GET", "POST"]:
                return
            body = await request.json()

            if body:
                request_logger.info("=============")
                request_logger.info(status_code)
                for key, val in body.items():
                    request_logger.info(f"{key}: {val}")

        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            try:
                response: Response = await original_route_handler(request)
                await _log_request(request, response.status_code)

                return response
            except HTTPException as e:
                await _log_request(request, e.status_code)
                raise e

        return custom_route_handler


pool = None


@asynccontextmanager
async def lifespan(app: FastAPI):
    global pool
    pool = ConnectionPool.from_url(config.CACHE_REDIS_URL)
    # await FastAPILimiter.init(await Redis.from_pool(pool))
    yield
    await pool.disconnect()


app = FastAPI(debug=False, lifespan=lifespan)
app.mount("/static", StaticFiles(directory="static"), name="static")
app.mount("/output", StaticFiles(directory="output"), name="output")
templates = Jinja2Templates(directory="templates")
router = APIRouter(route_class=LoggingRoute)


async def init_redis_pool() -> AsyncIterator[Redis]:
    async with Redis.from_url(config.CACHE_REDIS_URL) as redis:
        yield redis


async def http_client() -> AsyncIterator[AsyncClient]:
    async with AsyncClient() as c:
        yield c


error_logger = Logger("chart_tool.errors")
error_handler = RotatingFileHandler(
    "logs/error.log", maxBytes=1024 * 1024, backupCount=10
)
error_logger.addHandler(error_handler)


@router.get("/", response_class=HTMLResponse)
async def index(request: Request):
    return templates.TemplateResponse(
        "index.html", {"request": request, "sources": ImageSourceType.all()}
    )


class InputRequest(BaseModel):
    input: str
    columns: int
    source_type: ImageSourceType


class Job(BaseModel):
    id: UUID
    request: InputRequest


class JobStatus(BaseModel):
    status: Literal["running", "done", "error"]
    output: str | None


async def update_job_status(id: UUID, status: JobStatus, redis: Redis):
    await redis.set(f"job:{id}", status.model_dump_json(), ex=60 * 60)


async def get_job_status(id: UUID, redis: Redis) -> JobStatus | None:
    data = await redis.get(f"job:{id}")
    if data:
        try:
            res = JobStatus.model_validate_json(data)
            return res
        except ValidationError:
            return None
    return None


async def run_job(job: Job, redis: Redis, request: Request):
    await update_job_status(job.id, JobStatus(status="running", output=None), redis)
    async with AsyncClient() as client:
        try:
            io = await render_input_to_chart(
                job.request.input,
                job.request.source_type,
                job.request.columns,
                redis,
                client,
            )
            filename = f"{uuid4().hex}.png"
            filepath = path.join("output", filename)
            async with aiofiles.open(filepath, mode="wb") as f:
                await f.write(io.read())

                url = request.url_for("output", path=filename)
                await update_job_status(
                    job.id, JobStatus(status="done", output=f"{url}"), redis
                )
        except Exception as e:
            await update_job_status(
                job.id, JobStatus(status="error", output=f"{e}"), redis
            )


@router.post(
    "/chart",
    response_model=str,
    # dependencies=[Depends(RateLimiter(times=4, seconds=60))],
)
async def generate_chart(
    request: Request,
    body: InputRequest,
    background_tasks: BackgroundTasks,
    redis: Redis = Depends(init_redis_pool),
):
    job = Job(id=uuid4(), request=body)
    background_tasks.add_task(run_job, job, redis, request)
    return f"{job.id}"


@router.get(
    "/job/{job_id}",
    response_model=JobStatus,
)
async def job_status(
    job_id: UUID,
    redis: Redis = Depends(init_redis_pool),
):
    if status := await get_job_status(job_id, redis):
        return status
    raise HTTPException(status_code=404, detail="Job not found")


@router.get("/help", response_class=HTMLResponse)
async def help(request: Request):
    return templates.TemplateResponse("help.html", {"request": request})


app.include_router(router)


if __name__ == "__main__":
    run(app, host="0.0.0.0", port=8000)
